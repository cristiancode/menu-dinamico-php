<?php
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$menu = new \App\Controllers\MenuController();
$menuParents = $menu->parentMenus();
$response = $menu->edit($_GET['id']);
if(isset($_POST['update'])) {
	$menu->update($_POST);
	header("Location: /");
}
?>

<?php include "partials/header.php" ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Formulario</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
<div class="container pt-5">
    <form method="POST">
        <input type="hidden" name="id" value="<?= $response['menu']['id'] ?>">
        <input type="hidden" name="action" value="update">
        <div class="row mb-3">
            <label for="menuPadre" class="col-sm-2 col-form-label">Menu Padre:</label>
            <div class="col-sm-12 col-md-12 col-lg-4">
                <select class="form-select" name="menu_id" <?= $response['menu']['parent_id'] == 0 ? 'read-only' : '' ?> >
					<?php if( $response['menu']['parent_id'] == 0 ) { ?>
						<option value="0">---</option>
                    <?php
                        } else {
                            foreach ($response['menuParents'] as $row) {
                    ?>
                        <option value="<?= $row['id'] ?>" <?= $row['id'] == $response['menu']['parent_id'] ? 'selected' : '' ?> > <?= $row['nombre'] ?></option>
					<?php
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
            <div class="col-sm-12 col-md-12 col-lg-4">
                <input type="text" name="nombre" class="form-control" id="name" value="<?= $response['menu']['nombre']?>">
            </div>
        </div>
        <div class="row mb-3">
            <label for="name" class="col-sm-2 col-form-label">Descripcion:</label>
            <div class="col-sm-12 col-md-12 col-lg-4">
                <textarea class="form-control" name="descripcion" rows="5"><?= $response['menu']['descripcion']?></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <a href="/" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancelar</a>
            <button type="submit" name="update" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar Menu</button>
        </div>
    </form>
</div>
<?php include "partials/footer.php" ?>
