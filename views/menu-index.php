<?php include "partials/header.php" ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Evaluación</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <?php
			    foreach ($result['menu_all'] as $index => $menu) {
            ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <?= $menu['nombre'] ?>
                        </a>
                        <?php if( isset($menu['submenu']) && count($menu['submenu']) <> 0 ) { ?>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <?php foreach ($menu['submenu'] as $submenu) { ?>
                                    <li><a class="dropdown-item" href="#"><?= $submenu['nombre'] ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </li>
            <?php } ?>
        </ul>
    </div>
</nav>
<div class="container-fluid pt-5">
    <div class="mb-2">
        <a href="/menu-add.php?action=create" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</a>
    </div>
    <div class="col-sm-12 col-lg-12">
		<?php
		session_start();
		if(isset($_SESSION['message'])){
			?>
            <div class="alert alert-info text-center" style="margin-top:20px;">
				<?= $_SESSION['message']; ?>
            </div>
			<?php

			unset($_SESSION['message']);
		}
		?>
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <th>ID</th>
            <th>Nombre</th>
            <th>Menu Padre</th>
            <th>Descripción</th>
            <th>Acciónes</th>
            </thead>
            <tbody>
			<?php
			if( !empty($result)) {
				foreach ($result['all_menus'] as $row) {
					?>
                    <tr>
                        <td><?= $row['id']; ?></td>
                        <td><?= $row['nombre']; ?></td>
                        <td><?= $row['parent_id'] > 0 ? $row['parent_name'] : ''; ?></td>
                        <td><?= $row['descripcion']; ?></td>
                        <td>
                            <a href="index.php?action=edit&id=<?= $row['id'];?>" class="btn btn-success btn-sm" ><span class="glyphicon glyphicon-edit"></span> Editar</a>
                            <a href="index.php?action=delete&id=<?= $row['id'];?>" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Borrar</a>
                        </td>
                    </tr>
					<?php
				}
			}
			?>
            </tbody>
        </table>
    </div>
</div>
<?php include "partials/footer.php"; ?>
