<?php
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$menu = new \App\Controllers\MenuController();

/*if (!empty($_GET["action"])) {
    $menu->delete($_GET['id']);
    header("Location: /");
}*/

$result = $menu->index();
$show_menu = $menu->find($_GET['id']);
?>

<?php include "./partials/header.php" ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../index.php">Men&uacute;s Dinamicos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <?php
            foreach ($result['menu_all'] as $index => $menu) {
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <?= $menu['nombre'] ?>
                    </a>
                    <?php if( isset($menu['submenu']) && count($menu['submenu']) <> 0 ) { ?>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <?php foreach ($menu['submenu'] as $submenu) { ?>
                                <li><a class="dropdown-item" href="menu-show.php?id=<?= $submenu['id'] ?>"><?= $submenu['nombre'] ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</nav>
<div class="container pt-5">
    <div class="card">
        <div class="card-header">
            <?= $show_menu['nombre'] ?>
        </div>
        <div class="card-body">
            <h5 class="card-title"><?= $show_menu['descripcion'] ?></h5>
            <a href="../index.php" class="btn btn-primary">Listado de men&uacute;s</a>
        </div>
    </div>
</div>
<?php include "./partials/footer.php"; ?>
