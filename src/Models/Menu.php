<?php

namespace App\Models;

use App\Connection\Connection;
use App\Models\Abstracts\ModelAbstract;

class Menu extends ModelAbstract
{
	private int $id;

	private int $parentId;

	private string $nombre;

	private string $descripcion;

	private string $parent_name;

	protected Connection $conexion;

	public function __construct()
	{
		$this->conexion = new \App\Connection\Connection();
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getParentId(): string
	{
		return $this->parentId;
	}

	/**
	 * @param string $parentId
	 */
	public function setParentId(string $parentId): void
	{
		$this->parentId = $parentId;
	}

	/**
	 * @return string
	 */
	public function getNombre(): string
	{
		return $this->nombre;
	}

	/**
	 * @param string $nombre
	 */
	public function setNombre(string $nombre): void
	{
		$this->nombre = $nombre;
	}

	/**
	 * @return string
	 */
	public function getDescripcion(): string
	{
		return $this->descripcion;
	}

	/**
	 * @param string $descripcion
	 */
	public function setDescripcion(string $descripcion): void
	{
		$this->descripcion = $descripcion;
	}

	/**
	 * @return string
	 */
	public function getParentName(): string
	{
		return $this->parent_name;
	}

	/**
	 * @param string $parent_name
	 */
	public function setParentName(string $parent_name): void
	{
		$this->parent_name = $parent_name;
	}
}
