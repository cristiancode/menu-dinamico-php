<?php

namespace App\Models\Abstracts;

use App\Models\Contracts\ModelContract;

class ModelAbstract implements ModelContract
{

	public function all()
	{
		$query = 'SELECT * FROM menus';
		return $this->conexion->open()->query($query)->fetchAll();
	}

	public function store()
	{
		$db = $this->conexion->open();
		try {
			$stmt = $db->prepare("INSERT INTO menus (nombre, parent_id, descripcion, parent_name) VALUES (:nombre, :parentID, :descripcion, :parentName)");
			$_SESSION['message'] = ( $stmt->execute(array(':nombre' => $this->getNombre() , ':parentID' => $this->getParentId() , ':descripcion' => $this->getDescripcion(), ':parentName' => $this->getParentName()) ))
				? 'Menu guardado correctamente'
				: 'Algo salió mal. No se puede agregar menu';
		}catch (\PDOException $e) {
			$_SESSION['message'] = $e->getMessage();
		}
	}

	public function searchById()
	{
		$query = "SELECT * FROM menus WHERE id = " . $this->getId();
		return $this->conexion->open()->query($query)->fetch();
	}

	public function update()
	{
		$db = $this->conexion->open();
		try {
			$stmt = $db->prepare("UPDATE menus SET nombre = ? , parent_id = ?, descripcion = ?, parent_name = ? WHERE id = ?");
			$result = $stmt->execute([ $this->getNombre(), $this->getParentId(), $this->getDescripcion(), $this->getParentName(), $this->getId() ]);
			$_SESSION['message'] = ( $result ) ? 'Menú actualizado correctamente' : 'No se puso actualizar menú';
			return $result;
		} catch(\PDOException $e) {
			$_SESSION['message'] = $e->getMessage();
		}
	}

	public function delete()
	{
		$db = $this->conexion->open();
		try {
			$query = "DELETE FROM menus WHERE id =" . $this->getId();
			$_SESSION['message'] = ( $db->exec($query) ) ? 'Menú Eliminado' : 'Hubo un error al borrar menú';
		} catch (\PDOException $e) {
			$_SESSION['message'] = $e->getMessage();
		}
	}

	public function findWhere($column, $value)
	{
		$query = "SELECT * from menus WHERE {$column} = {$value}";
		return $this->conexion->open()->query($query)->fetchAll();
	}

	public function find($id)
	{
		$query = "SELECT * from menus WHERE id = {$id}";
		return $this->conexion->open()->query($query)->fetch();
	}
}
