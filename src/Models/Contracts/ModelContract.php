<?php
namespace App\Models\Contracts;

interface ModelContract
{
	public function all();
	public function store();
	public function searchById();
	public function findWhere($column, $value);
	public function find($id);
	public function update();
	public function delete();
}
