<?php

namespace App\Connection;

use PDO;
use PDOException;
use Symfony\Component\Dotenv\Dotenv;

class Connection
{
	private string $server = "";
	private string $username = "";
	private string $password = "";
	private array $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
	protected PDO $conn;

	public function __construct()
	{
		$dotenv = new Dotenv();
		$dotenv->load($_SERVER['DOCUMENT_ROOT'].'/.env');
		$this->server = "mysql:host=" . $_SERVER['DB_HOST'] . ";" . "dbname=" . $_SERVER['DB_DATABASE'];
		$this->username = $_SERVER['DB_USERNAME'];
		$this->password = $_SERVER['DB_PASSWORD'];
	}

	public function open(): PDO
    {
		try{
			$this->conn = new PDO($this->server, $this->username, $this->password, $this->options);
		}
		catch (PDOException $e){
			echo "Hubo un problema con la conexión: " . $e->getMessage();
		}

        return $this->conn;
	}

	/**
	 * @return void
	 */
	public function close(): void
	{
		$this->conn = null;
	}

}
