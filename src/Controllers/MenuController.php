<?php

namespace App\Controllers;

use App\Models\Menu;

class MenuController
{
	protected Menu $menu;

	public function __construct()
	{
		$this->menu = new Menu();
	}

	/**
	 * @return array|false
	 */
	public function index()
	{
		$menus = $this->menu->all();
		$menuAll = [];
		foreach ($menus as $index => $menu) {
			if( $menu['parent_id'] <> 0) {
				unset($menu[$index]);
			} else {
				$items = [array_merge($menu, ['submenu' => $this->getChildren($menus, $menu) ])];

				$menuAll = array_merge($menuAll, $items);
			}
		}
		return [
			'all_menus' => $menus,
			'menu_all' => $menuAll
		];
	}

	public function parentMenus()
	{
		return $this->menu->findWhere('parent_id', 0);
	}

	public function store(array $request): void
	{
		if( $request['menu_id'] <> 0) {
			$menu_parent = $this->menu->find($request['menu_id']);
			$this->menu->setParentName($menu_parent['nombre']);
		}
		$this->menu->setNombre($request['nombre']);
		$this->menu->setDescripcion($request['descripcion']);
		$this->menu->setParentId($request['menu_id']);
		$this->menu->store();
	}

	public function edit(int $id): array
	{
		$this->menu->setId($id);

		$response = [
			'menuParents' => $this->menu->findWhere('parent_id', 0),
			'menu' => $this->menu->searchById()
		];

		return $response;
	}

	public function update(array $request)
	{
		if( $request['menu_id'] > 0) {
			$menu_parent = $this->menu->find($request['menu_id']);
			$this->menu->setParentName($menu_parent['nombre']);
		}
		$this->menu->setId($request['id']);
		$this->menu->setNombre($request['nombre']);
		$this->menu->setDescripcion($request['descripcion']);
		$this->menu->setParentId($request['menu_id']);
		return $this->menu->update();
	}

	public function delete($id)
	{
		$this->menu->setId($id);
		$this->menu->delete();
	}


	private function getChildren($data, $line)
	{

		$children = [];
		foreach ($data as $line1) {

			if ($line['id'] == $line1['parent_id']) {
				$children = array_merge($children, [array_merge($line1, ['submenu' => $this->submenus($data, $line1)])]);
			}
		}

		return $children;
	}

	private function submenus($data, $line)
	{
		$children = [];
		foreach ($data as $line1) {

			if ($line['id'] == $line1['parent_id']) {
				$children = array_merge($children, [array_merge($line1, ['submenu' => $this->submenus($data, $line1)])]);
			}
		}
		return $children;
	}

    public function find($id)
    {
        return $this->menu->find($id);
    }
}
